package main

import (
  "net/http"
  "encoding/json"
  "log"

  "bitbucket.org/andreacaban/commowand-cd-pipeline/pkgb"
)

func pingHandler(writer http.ResponseWriter, req *http.Request) {
    js, err := json.Marshal("Pong")
    if err != nil {
      http.Error(writer, err.Error(), http.StatusInternalServerError)
      return
    }

    writer.Header().Set("Content-Type", "application/json")
    writer.Write(js)
}

func main() {
  http.HandleFunc("/ping", pingHandler)
  pkgb.Pkgb()
  log.Fatal(http.ListenAndServe(":8084", nil))
}
