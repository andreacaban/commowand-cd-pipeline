pipeline {
    agent none
    stages {
        stage('Installing deps') {
            agent {
                docker {
                    image 'golang:1.10.2-stretch'
                    args  '-v $WORKSPACE:/go/src/bitbucket.org/andreacaban/commowand-cd-pipeline -e "APP_DIR=/go/src/bitbucket.org/andreacaban/commowand-cd-pipeline"'
                    reuseNode true
                }
            }
            steps {
                sh 'cd $APP_DIR && go list ./...'
                sh 'cd $APP_DIR && go get -v'
            }
        }
        stage('Testing app') {
            agent {
                docker {
                    image 'golang:1.10.2-stretch'
                    args  '-v $WORKSPACE:/go/src/bitbucket.org/andreacaban/commowand-cd-pipeline -e "APP_DIR=/go/src/bitbucket.org/andreacaban/commowand-cd-pipeline"'
                    reuseNode true
                }
            }
            steps {
                sh 'cd $APP_DIR && go test'
            }
        }
        stage('Building app') {
            agent {
                docker {
                    image 'golang:1.10.2-stretch'
                    args  '-v $WORKSPACE:/go/src/bitbucket.org/andreacaban/commowand-cd-pipeline -e "APP_DIR=/go/src/bitbucket.org/andreacaban/commowand-cd-pipeline"'
                    reuseNode true
                }
            }
            steps {
                sh 'cd $APP_DIR && CGO_ENABLED=0 GOOS=linux go build -a -v -tags netgo .'
            }
        }
        stage('Building docker image') {
            agent any
            steps {
                script {
                    newImage = docker.build("andreacab/commowand-cd-pipeline:${env.GIT_COMMIT}")
                }
            }
        }
        stage('Checking docker image') {
            agent any
            steps {
                script {
                    newImage.withRun('-p 8084:8084') {
                        def res = sh (
                            script: 'curl http://localhost:8084/ping',
                            returnStdout: true
                        ).trim()

                        if (res.equals('\"Pong\"')) {
                            sh 'echo Success!'
                        } else {
                            error("Health check failed, received ${res} instead of Pong")
                        }
                    }
                }
            }
        }
        stage('Pushing to Dockerhub') {
            agent any
            steps {
                script {
                    newImage.push()
                    newImage.push('latest')
                }
            }
        }
    }
}