# Go example app for CI-CD with jenkins
## Objectives:
1. [x] trigger pipeline when changes are pushed onto bitbucket hello master branch
2. [x] Add test stage before building go app
3. [x] Cross compile/build the go app in a docker container (using Dockerfile-build or standard go image)
4. [x] Build new docker image on every build with new binary (using Dockerfile)
5. [x] Test image works and is running properly
6. [x] Publish new image to docker hub or amazon ECR
7. [ ] Deploy new image to AWS infrastructure
8. [x] Add health check for the image
9. [ ] Move to makefiles (i.e image test)

# Build go app locally
1. `$ CGO_ENABLED=0 GOOS=linux go build -a -v -tags netgo .`
2. `$ docker build -t hello .` 
3. `$ docker run -p 8084:8084 --rm hello`

## Notes
- CGO_ENABLED=0: disable cgo - necessary to have a statically linked binary
- GOOS-linux - necessary to build a binary that runx on a linux machine 
- Netgo - necessary to have a fully statically-linked binary as net/http is dynamically linked by the go compiler. Problem here seems to be the DNS resolver, see [here](https://blog.docker.com/2016/09/docker-golang/)

# Setup
1) Connect to bitbucket using the plugin xxxx
2) Add ssh key to jenkins server/user in order to access the bitbucket repo
