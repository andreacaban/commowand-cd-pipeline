FROM scratch
COPY ./commowand-cd-pipeline /commowand-cd-pipeline
EXPOSE 8084
ENTRYPOINT ["/commowand-cd-pipeline"]

